# Payrexx Java

## Usage

This library can be used similar to the official PHP library.

### Example 1: Get all Payment Providers
```java
// Initialize payrexx api
Payrexx payrexx = new Payrexx("your-instance-name", "your-supersecret-api-key");

// Prepare request to read all payment providers
PayrexxRequest pspRequest = new PayrexxRequest.Builder()
    .endpoint(PayrexxEndpoint.PAYMENT_PROVIDER)
    .action(PayrexxAction.GET)
    .build();

// Execute request
List<PaymentProviderResponse> pspResp = payrexx.doRequest(pspRequest, PaymentProviderResponse.class);

// Read values of specific response
System.out.println(pspResp.get(0).id());
System.out.println(pspResp.get(0).name());
```

### Example 2: Create Gateway with a lot of optional params
```java
// Initialize payrexx api
Payrexx payrexx = new Payrexx("your-instance-name", "your-supersecret-api-key");

// Prepare gateway request
GatewayRequest gatewayRequest = new GatewayRequest.Builder()
    .amount(1000)
    .currency("CHF")
    .vatRate(8.0)
    .sku("Stück")
    .purpose("Zweck")
    .successRedirectUrl("https://test.com/success")
    .failedRedirectUrl("https://test.com/failed")
    .cancelRedirectUrl("https://test.com/cancelled")
    .basket(List.of(new BasketItem.Builder()
            .amount(800)
            .name("Test 1")
            .quantity(1)
            .build(),
        new BasketItem.Builder()
            .amount(200)
            .name("Test 2")
            .quantity(1)
            .build()))
    .psp(List.of(27))
    .preAuthorization(false)
    .reservation(false)
    .referenceId("Reference12345")
    .field("forename", "Max")
    .field("surname", "Müller")
    .field("email", "test@email.com")
    .skipResultPage(false)
    .chargeOnAuthorization(false)
    .validity(10)
    .subscriptionState(false)
    .buttonText(List.of("Button Text"))
    .lookAndFeelProfile("2e56a19b")
    .successMessage("Great Success :-)")
    .build();

// Prepare request to create a gateway
PayrexxRequest req = new PayrexxRequest.Builder()
    .endpoint(PayrexxEndpoint.GATEWAY)
    .action(PayrexxAction.CREATE)
    .bodyData(gatewayRequest)
    .build();

// Execute request
List<GatewayResponse> gatewayResp = payrexx.doRequest(req, GatewayResponse.class);

// Read values of response
System.out.println(gatewayResp.get(0).id());
System.out.println(gatewayResp.get(0).status());
System.out.println(gatewayResp.get(0).link());
System.out.println(gatewayResp.get(0).referenceId());
```