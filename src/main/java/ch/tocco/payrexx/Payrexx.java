package ch.tocco.payrexx;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.client.utils.URIBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.common.escape.Escaper;

import ch.tocco.payrexx.model.response.PayrexxResponse;

import static ch.tocco.payrexx.helper.PayrexxEscapers.*;

public class Payrexx {
    private static final String DEFAULT_BASE_URL = "https://api.payrexx.com/v1.0";
    private final String baseUrl;
    private final String instance;
    private final String apiKey;

    private final HttpClient httpClient;

    private final ObjectMapper objectMapper;

    public Payrexx(String baseUrl, String instance, String apiKey, HttpClient httpClient, ObjectMapper objectMapper) {
        this.baseUrl = baseUrl;
        this.instance = instance;
        this.apiKey = apiKey;
        this.httpClient = httpClient;
        this.objectMapper = objectMapper;

        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public Payrexx(String baseUrl, String instance, String apiKey) {
        this(baseUrl, instance, apiKey, HttpClient.newHttpClient(), new ObjectMapper());
    }

    public Payrexx(String instance, String apiKey) {
        this(DEFAULT_BASE_URL, instance, apiKey);
    }

    public <T> List<T> doRequest(PayrexxRequest req, Class<T> clazz) throws PayrexxException {
        try {
            URIBuilder uriBuilder = getUriBuilder(req);

            HttpRequest.BodyPublisher bodyPublisher = HttpRequest.BodyPublishers.noBody();
            Map<String, String> bodyData = req.bodyData();

            String apiSignature = getApiSignature(bodyData);
            if ("GET".equals(req.method()) && (bodyData == null || bodyData.isEmpty())) {
                uriBuilder.addParameter("ApiSignature", apiSignature);
            } else {
                if (bodyData == null || bodyData.isEmpty()) {
                    bodyData = Maps.newLinkedHashMap();
                }
                bodyData.put("ApiSignature", apiSignature);
                bodyPublisher = HttpRequest.BodyPublishers.ofString(getPayloadEncoded(bodyData, RFC3986_ESCAPER));
            }

            HttpRequest httpRequest = HttpRequest.newBuilder(uriBuilder.build())
                .method(req.method(), bodyPublisher)
                .build();

            HttpResponse<InputStream> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofInputStream());

            try (InputStream body = response.body()) {
                PayrexxResponse payrexxResponse = objectMapper.readValue(body, PayrexxResponse.class);
                if ("error".equals(payrexxResponse.status())) {
                    throw new PayrexxException(payrexxResponse.message());
                }
                return parseData(payrexxResponse, clazz);
            }
        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new PayrexxException(e);
        }
    }

    private URIBuilder getUriBuilder(PayrexxRequest req) throws URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder(baseUrl);

        List<String> pathSegments = uriBuilder.getPathSegments();
        pathSegments.add(req.endpoint());
        if (!Strings.isNullOrEmpty(req.id())) {
            pathSegments.add(req.id());

            if (req.withDetails()) {
                pathSegments.add("details");
            }
        }

        uriBuilder
            .setPathSegments(pathSegments)
            .addParameter("instance", instance);

        return uriBuilder;
    }

    private String getPayloadEncoded(Map<String, String> values, Escaper escaper) {
        return values.entrySet().stream()
            .map(v -> String.format("%s=%s", escaper.escape(v.getKey()), escaper.escape(v.getValue())))
            .collect(Collectors.joining("&"));
    }

    private String getApiSignature(Map<String, String> bodyData) throws PayrexxException {
        if (bodyData == null || bodyData.isEmpty()) {
            return getApiSignature("");
        } else {
            return getApiSignature(getPayloadEncoded(bodyData, RFC1738_ESCAPER));
        }
    }

    private String getApiSignature(String payload) throws PayrexxException {
        try {
            Mac hmacSHA256 = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(apiKey.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            hmacSHA256.init(secretKeySpec);
            byte[] signature = hmacSHA256.doFinal(payload.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(signature);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new PayrexxException(e);
        }
    }

    private <T> List<T> parseData(PayrexxResponse response, Class<T> clazz) {
        if ("success".equals(response.status())) {
            if (response.data() != null) {
                return response.data().stream()
                    .map(o -> objectMapper.convertValue(o, clazz))
                    .toList();
            }
        }
        return Collections.emptyList();
    }
}
