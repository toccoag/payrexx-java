package ch.tocco.payrexx;

public class PayrexxException extends Exception {
    public PayrexxException(String message) {
        super(message);
    }

    public PayrexxException(Throwable e) {
        super(e);
    }
}
