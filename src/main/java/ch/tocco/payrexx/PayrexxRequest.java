package ch.tocco.payrexx;

import java.util.Map;

import com.google.common.base.Strings;

import ch.tocco.payrexx.model.request.RequestModel;
import ch.tocco.payrexx.types.PayrexxAction;
import ch.tocco.payrexx.types.PayrexxEndpoint;

public record PayrexxRequest(String endpoint, String method, String id, Map<String, String> bodyData, boolean withDetails) {

    public static class Builder {
        private String endpoint;
        private String method;
        private String id;
        private Map<String, String> bodyData;
        private boolean withDetails;

        public Builder endpoint(String endpoint) {
            this.endpoint = endpoint;
            return this;
        }

        public Builder endpoint(PayrexxEndpoint endpoint) {
            this.endpoint = endpoint.toString();
            return this;
        }

        public Builder action(PayrexxAction action) {
            this.method = action.getMethod();
            this.withDetails = action.isWithDetails();
            return this;
        }

        public Builder method(String method) {
            this.method = method;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder bodyData(RequestModel requestBean) {
            this.bodyData = requestBean.getValueMap();
            return this;
        }

        public PayrexxRequest build() {
            assert !Strings.isNullOrEmpty(endpoint) : "endpoint is required!";
            assert !Strings.isNullOrEmpty(method) : "method is required!";
            return new PayrexxRequest(endpoint, method, id, bodyData, withDetails);
        }
    }
}
