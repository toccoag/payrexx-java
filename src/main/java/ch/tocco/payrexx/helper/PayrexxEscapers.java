package ch.tocco.payrexx.helper;

import com.google.common.escape.Escaper;
import com.google.common.net.PercentEscaper;

public final class PayrexxEscapers {
    private static final String SAFE_CHARS = "-_.*";

    public static Escaper RFC1738_ESCAPER = new PercentEscaper(SAFE_CHARS, true);
    public static Escaper RFC3986_ESCAPER = new PercentEscaper(SAFE_CHARS, false);
}
