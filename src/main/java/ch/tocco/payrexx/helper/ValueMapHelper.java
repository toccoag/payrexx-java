package ch.tocco.payrexx.helper;

import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.jetbrains.annotations.Nullable;

import ch.tocco.payrexx.model.request.BasketItem;
import ch.tocco.payrexx.model.request.Field;

public class ValueMapHelper {
    public static void addOptional(Map<String, String> targetMap, String fieldName, @Nullable Object value) {
        if (value != null && (!(value instanceof String) || !((String) value).isEmpty())) {
            add(targetMap, fieldName, value);
        }
    }

    public static void add(Map<String, String> targetMap, String fieldName, Object value) {
        targetMap.put(fieldName, value.toString());
    }

    public static void addOptionalList(Map<String, String> targetMap, String fieldName, @Nullable List values) {
        if (values != null && !values.isEmpty()) {
            addList(targetMap, fieldName, values);
        }
    }

    public static void addList(Map<String, String> targetMap, String fieldName, List values) {
        IntStream.range(0, values.size())
            .forEach(i -> add(targetMap, getListFieldName(fieldName, i), values.get(i)));
    }

    private static String getListFieldName(String fieldName, int i) {
        return String.format("%s[%d]", fieldName, i);
    }

    public static void addBasket(Map<String, String> targetMap, String fieldName, @Nullable List<BasketItem> basket) {
        if (basket != null && !basket.isEmpty()) {
            for (int i = 0; i < basket.size(); i++) {
                BasketItem basketItem = basket.get(i);
                add(targetMap, getBasketItemFieldName(fieldName, i, "name"), basketItem.name());
                add(targetMap, getBasketItemFieldName(fieldName, i, "quantity"), basketItem.quantity());
                add(targetMap, getBasketItemFieldName(fieldName, i, "amount"), basketItem.amount());

                addOptional(targetMap, getBasketItemFieldName(fieldName, i, "description"), basketItem.description());
                addOptional(targetMap, getBasketItemFieldName(fieldName, i, "vatRate"), basketItem.vatRate());
            }
        }
    }

    private static String getBasketItemFieldName(String fieldName, int i, String basketFieldName) {
        return String.format("%s[%d][%s]", fieldName, i, basketFieldName);
    }

    public static void addFields(Map<String, String> targetMap, String fieldName, @Nullable List<Field> fields) {
        if (fields != null && !fields.isEmpty()) {
            for (Field field : fields) {
                addOptional(targetMap, getFieldFieldName(fieldName, field.name(), "value"), field.value());
                addOptional(targetMap, getFieldFieldName(fieldName, field.name(), "defaultValue"), field.defaultValue());
                addOptional(targetMap, getFieldFieldName(fieldName, field.name(), "mandatory"), field.mandatory());
            }
        }
    }

    private static String getFieldFieldName(String fieldName, String fieldFieldName, String propertyName) {
        return String.format("%s[%s][%s]", fieldName, fieldFieldName, propertyName);
    }
}
