package ch.tocco.payrexx.model.request;

import com.google.common.base.Strings;

public record BasketItem(String name, String description, int quantity, int amount, Double vatRate) {

    public static class Builder {
        private String name;
        private String description;
        private Integer quantity;
        private Integer amount;
        private Double vatRate;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder quantity(int quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder amount(int amount) {
            this.amount = amount;
            return this;
        }

        public Builder vatRate(Double vatRate) {
            this.vatRate = vatRate;
            return this;
        }

        public BasketItem build() {
            assert !Strings.isNullOrEmpty(name) : "name is required";
            assert amount != null : "amount is required";
            assert quantity != null : "quantity is required";
            return new BasketItem(name, description, quantity, amount, vatRate);
        }
    }
}
