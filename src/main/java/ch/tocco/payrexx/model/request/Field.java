package ch.tocco.payrexx.model.request;

public record Field(String name,
                    String value,
                    String defaultValue,
                    Boolean mandatory) {
    public Field(String name, String value) {
        this(name, value, null, null);
    }

    public Field(String name, Boolean mandatory) {
        this(name, null, null, mandatory);
    }

    public Field(String name, String defaultValue, Boolean mandatory) {
        this(name, null, defaultValue, mandatory);
    }
}
