package ch.tocco.payrexx.model.request;

import java.util.List;
import java.util.Map;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import static ch.tocco.payrexx.helper.ValueMapHelper.*;

public record GatewayRequest(int amount,
                             String currency,
                             Double vatRate,
                             String sku,
                             String purpose,
                             String successRedirectUrl,
                             String failedRedirectUrl,
                             String cancelRedirectUrl,
                             List<BasketItem> basket,
                             List<Integer> psp,
                             List<String> pm,
                             Boolean preAuthorization,
                             Boolean reservation,
                             String referenceId,
                             List<Field> fields,
                             Boolean skipResultPage,
                             Boolean chargeOnAuthorization,
                             Integer validity,
                             Boolean subscriptionState,
                             String subscriptionInterval,
                             String subscriptionPeriod,
                             String subscriptionCancellationInterval,
                             List<String> buttonText,
                             String lookAndFeelProfile,
                             String successMessage,
                             String qrCodeSessionId) implements RequestModel {

    @Override
    public Map<String, String> getValueMap() {
        Map<String, String> values = Maps.newLinkedHashMap();
        add(values, "model", "Gateway");

        int amount = amount();
        add(values, "amount", amount());
        add(values, "currency", currency());

        addOptional(values, "vatRate", vatRate());
        addOptional(values, "sku", sku());
        addOptional(values, "purpose", purpose());
        addOptional(values, "successRedirectUrl", successRedirectUrl());
        addOptional(values, "failedRedirectUrl", failedRedirectUrl());
        addOptional(values, "cancelRedirectUrl", cancelRedirectUrl());
        addBasket(values, "basket", basket());
        addOptionalList(values, "psp", psp());
        addOptionalList(values, "pm", pm());
        addOptional(values, "preAuthorization", preAuthorization());
        addOptional(values, "reservation", reservation());
        addOptional(values, "referenceId", referenceId());
        addFields(values, "fields", fields());
        addOptional(values, "skipResultPage", skipResultPage());
        addOptional(values, "chargeOnAuthorization", chargeOnAuthorization());
        addOptional(values, "validity", validity());
        addOptional(values, "subscriptionState", subscriptionState());
        addOptional(values, "subscriptionInterval", subscriptionInterval());
        addOptional(values, "subscriptionPeriod", subscriptionPeriod());
        addOptional(values, "subscriptionCancellationInterval", subscriptionCancellationInterval());
        addOptionalList(values, "buttonText", buttonText());
        addOptional(values, "lookAndFeelProfile", lookAndFeelProfile());
        addOptional(values, "successMessage", successMessage());

        return values;
    }


    public static final class Builder {
        private Integer amount;
        private String currency;
        private Double vatRate;
        private String sku;
        private String purpose;
        private String successRedirectUrl;
        private String failedRedirectUrl;
        private String cancelRedirectUrl;
        private List<BasketItem> basket;
        private List<Integer> psp;
        private List<String> pm;
        private Boolean preAuthorization;
        private Boolean reservation;
        private String referenceId;
        private final List<Field> fields = Lists.newArrayList();
        private Boolean skipResultPage;
        private Boolean chargeOnAuthorization;
        private Integer validity;
        private Boolean subscriptionState;
        private String subscriptionInterval;
        private String subscriptionPeriod;
        private String subscriptionCancellationInterval;
        private List<String> buttonText;
        private String lookAndFeelProfile;
        private String successMessage;
        private String qrCodeSessionId;

        public Builder amount(Integer amount) {
            this.amount = amount;
            return this;
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder vatRate(Double vatRate) {
            this.vatRate = vatRate;
            return this;
        }

        public Builder sku(String sku) {
            this.sku = sku;
            return this;
        }

        public Builder purpose(String purpose) {
            this.purpose = purpose;
            return this;
        }

        public Builder successRedirectUrl(String successRedirectUrl) {
            this.successRedirectUrl = successRedirectUrl;
            return this;
        }

        public Builder failedRedirectUrl(String failedRedirectUrl) {
            this.failedRedirectUrl = failedRedirectUrl;
            return this;
        }

        public Builder cancelRedirectUrl(String cancelRedirectUrl) {
            this.cancelRedirectUrl = cancelRedirectUrl;
            return this;
        }

        public Builder basket(List<BasketItem> basket) {
            this.basket = basket;
            return this;
        }

        public Builder psp(List<Integer> psp) {
            this.psp = psp;
            return this;
        }

        public Builder pm(List<String> pm) {
            this.pm = pm;
            return this;
        }

        public Builder preAuthorization(Boolean preAuthorization) {
            this.preAuthorization = preAuthorization;
            return this;
        }

        public Builder reservation(Boolean reservation) {
            this.reservation = reservation;
            return this;
        }

        public Builder referenceId(String referenceId) {
            this.referenceId = referenceId;
            return this;
        }

        public Builder field(String fieldName, String value) {
            this.fields.add(new Field(fieldName, value));
            return this;
        }

        public Builder skipResultPage(Boolean skipResultPage) {
            this.skipResultPage = skipResultPage;
            return this;
        }

        public Builder chargeOnAuthorization(Boolean chargeOnAuthorization) {
            this.chargeOnAuthorization = chargeOnAuthorization;
            return this;
        }

        public Builder validity(Integer validity) {
            this.validity = validity;
            return this;
        }

        public Builder subscriptionState(Boolean subscriptionState) {
            this.subscriptionState = subscriptionState;
            return this;
        }

        public Builder subscriptionInterval(String subscriptionInterval) {
            this.subscriptionInterval = subscriptionInterval;
            return this;
        }

        public Builder subscriptionPeriod(String subscriptionPeriod) {
            this.subscriptionPeriod = subscriptionPeriod;
            return this;
        }

        public Builder subscriptionCancellationInterval(String subscriptionCancellationInterval) {
            this.subscriptionCancellationInterval = subscriptionCancellationInterval;
            return this;
        }

        public Builder buttonText(List<String> buttonText) {
            this.buttonText = buttonText;
            return this;
        }

        public Builder lookAndFeelProfile(String lookAndFeelProfile) {
            this.lookAndFeelProfile = lookAndFeelProfile;
            return this;
        }

        public Builder successMessage(String successMessage) {
            this.successMessage = successMessage;
            return this;
        }

        public Builder qrCodeSessionId(String qrCodeSessionId) {
            this.qrCodeSessionId = qrCodeSessionId;
            return this;
        }

        public GatewayRequest build() {
            assert amount != null : "amount is required";
            assert !Strings.isNullOrEmpty(currency) : "currency is required";
            return new GatewayRequest(amount, currency, vatRate, sku, purpose, successRedirectUrl, failedRedirectUrl, cancelRedirectUrl, basket, psp, pm, preAuthorization, reservation, referenceId, fields, skipResultPage, chargeOnAuthorization, validity, subscriptionState, subscriptionInterval, subscriptionPeriod, subscriptionCancellationInterval, buttonText, lookAndFeelProfile, successMessage, qrCodeSessionId);
        }
    }
}