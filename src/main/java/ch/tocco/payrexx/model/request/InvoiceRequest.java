package ch.tocco.payrexx.model.request;

import java.util.List;
import java.util.Map;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import static ch.tocco.payrexx.helper.ValueMapHelper.*;

public record InvoiceRequest(String title,
                             String description,
                             String referenceId,
                             String purpose,
                             int amount,
                             String currency,
                             Double vatRate,
                             List<Integer> psp,
                             List<String> pm,
                             String sku,
                             Boolean preAuthorization,
                             Boolean reservation,
                             String name,
                             List<Field> fields,
                             Boolean hideFields,
                             String concardisOrderId,
                             String buttonText,
                             String expirationDate,
                             String successRedirectUrl,
                             String failedRedirectUrl,
                             Boolean subscriptionState,
                             String subscriptionInterval,
                             String subscriptionPeriod,
                             String subscriptionCancellationInterval) implements RequestModel {

    @Override
    public Map<String, String> getValueMap() {
        Map<String, String> values = Maps.newLinkedHashMap();
        add(values, "model", "Invoice");

        add(values, "title", title());
        add(values, "description", description());
        add(values, "referenceId", referenceId());
        add(values, "purpose", purpose());
        add(values, "amount", amount());
        add(values, "currency", currency());

        addOptional(values, "vatRate", vatRate());
        addOptionalList(values, "psp", psp());
        addOptionalList(values, "pm", pm());
        addOptional(values, "sku", sku());
        addOptional(values, "preAuthorization", preAuthorization());
        addOptional(values, "reservation", reservation());
        addOptional(values, "name", name());
        addFields(values, "fields", fields());
        addOptional(values, "hideFields", hideFields());
        addOptional(values, "concardisOrderId", concardisOrderId());
        addOptional(values, "buttonText", buttonText());
        addOptional(values, "expirationDate", expirationDate());
        addOptional(values, "successRedirectUrl", successRedirectUrl());
        addOptional(values, "failedRedirectUrl", failedRedirectUrl());
        addOptional(values, "subscriptionState", subscriptionState());
        addOptional(values, "subscriptionInterval", subscriptionInterval());
        addOptional(values, "subscriptionPeriod", subscriptionPeriod());
        addOptional(values, "subscriptionCancellationInterval", subscriptionCancellationInterval());

        return values;
    }

    public static final class Builder {
        private String title;
        private String description;
        private String referenceId;
        private String purpose;
        private Integer amount;
        private String currency;
        private Double vatRate;
        private List<Integer> psp;
        private List<String> pm;
        private String sku;
        private Boolean preAuthorization;
        private Boolean reservation;
        private String name;
        private final List<Field> fields = Lists.newArrayList();
        private Boolean hideFields;
        private String concardisOrderId;
        private String buttonText;
        private String expirationDate;
        private String successRedirectUrl;
        private String failedRedirectUrl;
        private Boolean subscriptionState;
        private String subscriptionInterval;
        private String subscriptionPeriod;
        private String subscriptionCancellationInterval;

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder referenceId(String referenceId) {
            this.referenceId = referenceId;
            return this;
        }

        public Builder purpose(String purpose) {
            this.purpose = purpose;
            return this;
        }

        public Builder amount(int amount) {
            this.amount = amount;
            return this;
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder vatRate(Double vatRate) {
            this.vatRate = vatRate;
            return this;
        }

        public Builder psp(List<Integer> psp) {
            this.psp = psp;
            return this;
        }

        public Builder pm(List<String> pm) {
            this.pm = pm;
            return this;
        }

        public Builder sku(String sku) {
            this.sku = sku;
            return this;
        }

        public Builder preAuthorization(Boolean preAuthorization) {
            this.preAuthorization = preAuthorization;
            return this;
        }

        public Builder reservation(Boolean reservation) {
            this.reservation = reservation;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder field(String name, boolean mandatory) {
            this.fields.add(new Field(name, mandatory));
            return this;
        }

        public Builder field(String name, boolean mandatory, String defaultValue) {
            this.fields.add(new Field(name, defaultValue, mandatory));
            return this;
        }

        public Builder hideFields(Boolean hideFields) {
            this.hideFields = hideFields;
            return this;
        }

        public Builder concardisOrderId(String concardisOrderId) {
            this.concardisOrderId = concardisOrderId;
            return this;
        }

        public Builder buttonText(String buttonText) {
            this.buttonText = buttonText;
            return this;
        }

        public Builder expirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public Builder successRedirectUrl(String successRedirectUrl) {
            this.successRedirectUrl = successRedirectUrl;
            return this;
        }

        public Builder failedRedirectUrl(String failedRedirectUrl) {
            this.failedRedirectUrl = failedRedirectUrl;
            return this;
        }

        public Builder subscriptionState(Boolean subscriptionState) {
            this.subscriptionState = subscriptionState;
            return this;
        }

        public Builder subscriptionInterval(String subscriptionInterval) {
            this.subscriptionInterval = subscriptionInterval;
            return this;
        }

        public Builder subscriptionPeriod(String subscriptionPeriod) {
            this.subscriptionPeriod = subscriptionPeriod;
            return this;
        }

        public Builder subscriptionCancellationInterval(String subscriptionCancellationInterval) {
            this.subscriptionCancellationInterval = subscriptionCancellationInterval;
            return this;
        }

        public InvoiceRequest build() {
            assert !Strings.isNullOrEmpty(title) : "title is required";
            assert !Strings.isNullOrEmpty(description) : "description is required";
            assert !Strings.isNullOrEmpty(referenceId) : "referenceId is required";
            assert !Strings.isNullOrEmpty(purpose) : "purpose is required";
            assert amount != null : "amount is required";
            assert !Strings.isNullOrEmpty(currency) : "currency is required";
            return new InvoiceRequest(title, description, referenceId, purpose, amount, currency, vatRate, psp, pm, sku, preAuthorization, reservation, name, fields, hideFields, concardisOrderId, buttonText, expirationDate, successRedirectUrl, failedRedirectUrl, subscriptionState, subscriptionInterval, subscriptionPeriod, subscriptionCancellationInterval);
        }
    }
}