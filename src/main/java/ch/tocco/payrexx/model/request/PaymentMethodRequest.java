package ch.tocco.payrexx.model.request;

import java.util.Map;

import com.google.common.collect.Maps;

import static ch.tocco.payrexx.helper.ValueMapHelper.*;

public record PaymentMethodRequest(String filterCurrency,
                                   String filterPaymentType,
                                   Integer filterPsp) implements RequestModel {

    @Override
    public Map<String, String> getValueMap() {
        Map<String, String> values = Maps.newLinkedHashMap();
        add(values, "model", "PaymentMethod");
        addOptional(values, "filterCurrency", filterCurrency());
        addOptional(values, "filterPaymentType", filterPaymentType());
        addOptional(values, "filterPsp", filterPsp());
        return values;
    }

    public static final class Builder {
        private String filterCurrency;
        private String filterPaymentType;
        private Integer filterPsp;

        public Builder filterCurrency(String filterCurrency) {
            this.filterCurrency = filterCurrency;
            return this;
        }

        public Builder filterPaymentType(String filterPaymentType) {
            this.filterPaymentType = filterPaymentType;
            return this;
        }

        public Builder filterPsp(Integer filterPsp) {
            this.filterPsp = filterPsp;
            return this;
        }

        public PaymentMethodRequest build() {
            return new PaymentMethodRequest(filterCurrency, filterPaymentType, filterPsp);
        }
    }
}
