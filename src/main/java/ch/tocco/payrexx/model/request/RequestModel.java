package ch.tocco.payrexx.model.request;

import java.util.Map;

public interface RequestModel {
    Map<String, String> getValueMap();
}
