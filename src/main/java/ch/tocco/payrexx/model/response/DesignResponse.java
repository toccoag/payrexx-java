package ch.tocco.payrexx.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record DesignResponse(
    String uuid,
    @JsonProperty("default")
    boolean isDefault,
    String name,
    String fontFamily,
    String fontSize,
    String textColor,
    String textColorVpos,
    String linkColor,
    String linkHoverColor,
    String buttonColor,
    String buttonHoverColor,
    String background,
    String backgroundColor,
    String headerBackground,
    String headerBackgroundColor,
    String emailHeaderBackgroundColor,
    String headerImageShape,
    boolean useIndividualEmailLogo,
    String logoBackgroundColor,
    String logoBorderColor,
    String VPOSGradientColor1,
    String VPOSGradientColor2,
    boolean enableRoundedCorners,
    String headerImage,
    String backgroundImage,
    String headerBackgroundImage,
    String emailHeaderImage,
    boolean headerImageCustomLink) {
}
