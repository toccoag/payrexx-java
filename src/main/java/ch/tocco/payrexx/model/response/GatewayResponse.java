package ch.tocco.payrexx.model.response;

public record GatewayResponse(
    long id,
    String status,
    String hash,
    String referenceId,
    String link,
    boolean preAuthorization,
    int amount,
    String currency,
    double vatRate,
    String sku,
    long createdAt,
    long requestId) {
}
