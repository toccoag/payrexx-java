package ch.tocco.payrexx.model.response;

import java.util.List;

public record InvoiceResponse(long id,
                              String status,
                              String hash,
                              String referenceId,
                              String link,
                              boolean preAuthorization,
                              String name,
                              boolean api,
                              List<Integer> psp,
                              List<String> pm,
                              int amount,
                              Double vatRate,
                              String currency,
                              String sku) {
}
