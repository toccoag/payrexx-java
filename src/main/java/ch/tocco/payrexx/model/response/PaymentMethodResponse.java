package ch.tocco.payrexx.model.response;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public record PaymentMethodResponse(String id,
                                    String name,
                                    Map<String, String> label,
                                    Map<String, String> logo,
                                    @JsonProperty("options_by_psp") Map<String, PspOptions> optionsByPsp) {
    public record PspOptions(String mode, 
                             @JsonProperty("payment_types") List<String> paymentTypes,
                             List<String> currencies) {
    }
}
