package ch.tocco.payrexx.model.response;

import java.util.List;


public record PaymentProviderResponse(long id, String name, List<String> paymentMethods, List<String> activePaymentMethods) {
}