package ch.tocco.payrexx.model.response;

import java.util.List;
import java.util.Map;

public record PayrexxResponse(String status, String message, List<Map<String, Object>> data) {
}
