package ch.tocco.payrexx.model.response;

public record SignatureCheckResponse(int id) {
}
