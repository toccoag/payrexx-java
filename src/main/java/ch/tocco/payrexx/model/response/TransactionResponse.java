package ch.tocco.payrexx.model.response;

public record TransactionResponse(long id,
                                  String uuid,
                                  int amount,
                                  String referenceId,
                                  String time,
                                  String status,
                                  String lang,
                                  String psp,
                                  int pspId,
                                  String mode) {
}
