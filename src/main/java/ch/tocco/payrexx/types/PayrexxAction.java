package ch.tocco.payrexx.types;

public enum PayrexxAction {
    CREATE("POST", false),
    CHARGE("POST", false),
    REFUND("POST", false),
    CAPTURE("POST", false),
    RECEIPT("POST", false),
    PRE_AUTHORIZE("POST", false),
    CANCEL("DELETE", false),
    DELETE("DELETE", false),
    UPDATE("PUT", false),
    GET("GET", false),
    GET_DETAILS("GET", true);

    private final String method;
    private final boolean withDetails;

    PayrexxAction(String method, boolean withDetails) {
        this.method = method;
        this.withDetails = withDetails;
    }

    public String getMethod() {
        return method;
    }

    public boolean isWithDetails() {
        return withDetails;
    }
}