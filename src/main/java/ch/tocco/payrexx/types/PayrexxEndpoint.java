package ch.tocco.payrexx.types;

public enum PayrexxEndpoint {
    SIGNATURE_CHECK("SignatureCheck"),
    SUBSCRIPTION("Subscription"),
    GATEWAY("Gateway"),
    TRANSACTION("Transaction"),
    DESIGN("Design"),
    INVOICE("Invoice"),
    PAYMENT_PROVIDER("PaymentProvider"),
    PAYMENT_METHOD("PaymentMethod"),
    QR_CODE("QrCode"),
    QR_CODE_SCAN("QrCodeScan"),
    PAYOUT("Payout");

    private final String endpoint;

    PayrexxEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String toString() {
        return this.endpoint;
    }
}