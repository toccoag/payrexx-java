open module payrexx.java {
    exports ch.tocco.payrexx;
    exports ch.tocco.payrexx.helper;
    exports ch.tocco.payrexx.model.request;
    exports ch.tocco.payrexx.model.response;
    exports ch.tocco.payrexx.types;

    requires com.fasterxml.jackson.databind;
    requires com.google.common;
    requires java.net.http;
    requires org.apache.httpcomponents.httpclient;
    requires org.jetbrains.annotations;
}