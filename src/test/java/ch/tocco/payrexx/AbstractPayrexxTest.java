package ch.tocco.payrexx;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Flow;

import org.testng.annotations.*;
import org.easymock.Capture;
import org.easymock.IMocksControl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import static com.google.common.truth.Truth.*;
import static org.easymock.EasyMock.*;

public abstract class AbstractPayrexxTest {
    protected final IMocksControl control = createControl();
    protected final HttpClient client = setupMock(HttpClient.class);

    protected <T> T setupMock(Class<T> clazz) {
        return control.mock(clazz);
    }

    @BeforeMethod
    public void cleanUp() {
        control.reset();
    }

    @AfterMethod
    public void verify() {
        control.verify();
    }

    protected Capture<HttpRequest> expectRequest(String filename) throws IOException, InterruptedException {
        Capture<HttpRequest> requestCapture = Capture.newInstance();
        HttpResponse<InputStream> response = control.createMock(HttpResponse.class);
        expect(response.body()).andReturn(this.getClass().getResourceAsStream(filename));
        expect(client.send(capture(requestCapture), eq(HttpResponse.BodyHandlers.ofInputStream()))).andReturn(response);
        return requestCapture;
    }

    protected <T> List<T> executeTestRequest(PayrexxRequest req, Class<T> clazz) throws PayrexxException {
        Payrexx payrexx = new Payrexx("https://test.com/v1.0",
            "test-instance",
            "this-is-a-secret-key",
            client,
            new ObjectMapper());

        control.replay();
        return payrexx.doRequest(req, clazz);
    }

    protected String retrieveBodyString(HttpRequest httpRequest) {
        assertThat(httpRequest.bodyPublisher().isPresent()).isTrue();
        FlowSubscriber<ByteBuffer> subscriber = new FlowSubscriber<>();
        HttpRequest.BodyPublisher bodyPublisher = httpRequest.bodyPublisher().get();
        bodyPublisher.subscribe(subscriber);
        return new String(subscriber.getBodyItems().get(0).array(), StandardCharsets.UTF_8);
    }

    protected void assertGetRequest(HttpRequest httpRequest, String expectedPath) {
        assertThat(httpRequest.method()).isEqualTo("GET");

        assertThat(httpRequest.uri().getPath()).isEqualTo(expectedPath);

        String[] queryParams = httpRequest.uri().getRawQuery().split("&");
        assertThat(queryParams[0]).isEqualTo("instance=test-instance");
    }

    protected void assertGetRequestWithoutBody(HttpRequest httpRequest, String expectedPath) {
        assertThat(httpRequest.method()).isEqualTo("GET");

        assertThat(httpRequest.uri().getPath()).isEqualTo(expectedPath);

        String[] queryParams = httpRequest.uri().getRawQuery().split("&");
        assertThat(queryParams[0]).isEqualTo("instance=test-instance");
        assertThat(queryParams[1]).isEqualTo("ApiSignature=N101CO%2FLQGEajGa9OUjApsz1%2Bof2ymLPvF%2FkNmY0U%2F4%3D");
    }

    protected void assertCreateRequest(HttpRequest httpRequest, String expectedPath) {
        // Method
        assertThat(httpRequest.method()).isEqualTo("POST");

        // Query Params
        assertThat(httpRequest.uri().getPath()).isEqualTo(expectedPath);
        assertThat(httpRequest.uri().getRawQuery()).isEqualTo("instance=test-instance");
    }

    protected void assertDeleteRequest(HttpRequest httpRequest, String expectedPath) {
        assertThat(httpRequest.method()).isEqualTo("DELETE");

        assertThat(httpRequest.uri().getPath()).isEqualTo(expectedPath);

        String[] queryParams = httpRequest.uri().getRawQuery().split("&");
        assertThat(queryParams[0]).isEqualTo("instance=test-instance");

        assertThat(retrieveBodyString(httpRequest)).isEqualTo("ApiSignature=N101CO%2FLQGEajGa9OUjApsz1%2Bof2ymLPvF%2FkNmY0U%2F4%3D");
    }

    protected String decode(String input) {
        return URLDecoder.decode(input, StandardCharsets.UTF_8);
    }

    private static class FlowSubscriber<T> implements Flow.Subscriber<T> {
        private final CountDownLatch latch = new CountDownLatch(1);
        private final List<T> bodyItems = Lists.newArrayList();

        public List<T> getBodyItems() {
            try {
                this.latch.await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return bodyItems;
        }

        @Override
        public void onSubscribe(Flow.Subscription subscription) {
            //Retrieve all parts
            subscription.request(Long.MAX_VALUE);
        }

        @Override
        public void onNext(T item) {
            this.bodyItems.add(item);
        }

        @Override
        public void onError(Throwable throwable) {
            this.latch.countDown();
        }

        @Override
        public void onComplete() {
            this.latch.countDown();
        }
    }
}
