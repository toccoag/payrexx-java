package ch.tocco.payrexx;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.util.List;

import org.testng.annotations.*;
import org.easymock.Capture;

import ch.tocco.payrexx.model.response.DesignResponse;
import ch.tocco.payrexx.types.PayrexxAction;
import ch.tocco.payrexx.types.PayrexxEndpoint;

import static com.google.common.truth.Truth.*;

public class DesignTest extends AbstractPayrexxTest {
    private static final DesignResponse DESIGN_RESPONSE = new DesignResponse(
        "2e56a19b",
        true,
        "Test",
        "Open Sans",
        "14",
        "24363a",
        "24363a",
        "0074d6",
        "2a6496",
        "00aff0",
        "7fba00",
        "color",
        "f9fafa",
        "color",
        "00aff0",
        "fafafa",
        "square",
        false,
        "00aff0",
        "dddddd",
        "ffffff",
        "ffffff",
        true,
        "payrexx800x300white.png",
        "",
        "",
        "",
        false
    );

    @Test
    public void testGetDesign() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("design_response.json");

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.DESIGN)
            .action(PayrexxAction.GET)
            .build();

        List<DesignResponse> response = executeTestRequest(req, DesignResponse.class);
        assertThat(response).hasSize(1);
        assertThat(response).containsExactly(DESIGN_RESPONSE);

        assertGetRequestWithoutBody(requestCapture.getValue(), "/v1.0/Design");
    }
}
