package ch.tocco.payrexx;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.util.List;

import org.testng.annotations.*;
import org.easymock.Capture;

import ch.tocco.payrexx.model.request.BasketItem;
import ch.tocco.payrexx.model.request.GatewayRequest;
import ch.tocco.payrexx.model.response.GatewayResponse;
import ch.tocco.payrexx.types.PayrexxAction;
import ch.tocco.payrexx.types.PayrexxEndpoint;

import static com.google.common.truth.Truth.*;

public class GatewayTest extends AbstractPayrexxTest {
    private static final GatewayResponse MINIMAL_RESPONSE = new GatewayResponse(123456789,
        "waiting",
        "25f9e794323b453885f5181f1b624d0b",
        "",
        "https://test-instance.payrexx.com/?payment=25f9e794323b453885f5181f1b624d0b",
        false,
        1000,
        "CHF",
        0.0,
        null,
        1700129055,
        123456789);

    private static final GatewayResponse EXTENDED_RESPONSE = new GatewayResponse(987654321,
        "waiting",
        "25f9e794323b453885f5181f1b624d0b",
        "Reference12345",
        "https://test-instance.payrexx.com/?payment=25f9e794323b453885f5181f1b624d0b",
        false,
        1000,
        "CHF",
        8.0,
        null,
        1700143115,
        987654321);

    private static final GatewayResponse CONFIRMED_RESPONSE = new GatewayResponse(123456789,
        "confirmed",
        "25f9e794323b453885f5181f1b624d0b",
        "",
        "https://test-instance.payrexx.com/?payment=25f9e794323b453885f5181f1b624d0b",
        false,
        1000,
        "CHF",
        0.0,
        null,
        1700129055,
        123456789);

    private static final GatewayResponse DELETE_RESPONSE = new GatewayResponse(123456789,
        null,
        null,
         null,
        null,
        false,
        0,
        null,
        0.0,
        null,
        0,
        0);

    @Test
    public void testCreateMinimalGateway() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("gateway_minimal_response.json");

        GatewayRequest gatewayRequest = new GatewayRequest.Builder()
            .amount(1000)
            .currency("CHF")
            .build();

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.GATEWAY)
            .action(PayrexxAction.CREATE)
            .bodyData(gatewayRequest)
            .build();

        List<GatewayResponse> resp = executeTestRequest(req, GatewayResponse.class);
        assertThat(resp).hasSize(1);
        assertThat(resp).containsExactly(MINIMAL_RESPONSE);

        HttpRequest httpRequest = requestCapture.getValue();
        assertCreateRequest(httpRequest, "/v1.0/Gateway");

        // Assert Body
        String bodyValue = retrieveBodyString(httpRequest);
        String[] params = bodyValue.split("&");

        assertThat(params[0]).isEqualTo("model=Gateway");
        assertThat(params[1]).isEqualTo("amount=1000");
        assertThat(params[2]).isEqualTo("currency=CHF");

        // API Signature
        assertThat(params[3]).isEqualTo("ApiSignature=QE2Hbd%2BZt8UmQ%2F0cJYbE9cMykp%2BMAp3EaBcgh3x2hKA%3D");
    }

    @Test
    public void testCreateExtendedGateway() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("gateway_extended_response.json");

        GatewayRequest gatewayRequest = new GatewayRequest.Builder()
            .amount(1000)
            .currency("CHF")
            .vatRate(8.0)
            .sku("Stück")
            .purpose("Zweck")
            .successRedirectUrl("https://test.com/success")
            .failedRedirectUrl("https://test.com/failed")
            .cancelRedirectUrl("https://test.com/cancelled")
            .basket(List.of(new BasketItem.Builder()
                    .amount(800)
                    .name("Test 1")
                    .quantity(1)
                    .build(),
                new BasketItem.Builder()
                    .amount(200)
                    .name("Test 2")
                    .quantity(1)
                    .build()))
            .psp(List.of(27))
            .preAuthorization(false)
            .reservation(false)
            .referenceId("Reference12345")
            .field("forename", "Max")
            .field("surname", "Müller")
            .field("email", "test@email.com")
            .skipResultPage(false)
            .chargeOnAuthorization(false)
            .validity(10)
            .subscriptionState(false)
            .buttonText(List.of("Button Text"))
            .lookAndFeelProfile("2e56a19b")
            .successMessage("Great Success :-)")
            .build();

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.GATEWAY)
            .action(PayrexxAction.CREATE)
            .bodyData(gatewayRequest)
            .build();

        List<GatewayResponse> resp = executeTestRequest(req, GatewayResponse.class);
        assertThat(resp).hasSize(1);
        assertThat(resp).containsExactly(EXTENDED_RESPONSE);

        HttpRequest httpRequest = requestCapture.getValue();
        assertCreateRequest(httpRequest, "/v1.0/Gateway");

        // Assert Body
        String bodyValue = retrieveBodyString(httpRequest);
        String[] params = bodyValue.split("&");

        assertThat(params[0]).isEqualTo("model=Gateway");
        assertThat(params[1]).isEqualTo("amount=1000");
        assertThat(params[2]).isEqualTo("currency=CHF");
        assertThat(params[3]).isEqualTo("vatRate=8.0");
        assertThat(params[4]).isEqualTo("sku=St%C3%BCck");
        assertThat(params[5]).isEqualTo("purpose=Zweck");

        // Redirect URLs
        assertThat(params[6]).isEqualTo("successRedirectUrl=https%3A%2F%2Ftest.com%2Fsuccess");
        assertThat(decode(params[6])).isEqualTo("successRedirectUrl=https://test.com/success");
        assertThat(params[7]).isEqualTo("failedRedirectUrl=https%3A%2F%2Ftest.com%2Ffailed");
        assertThat(decode(params[7])).isEqualTo("failedRedirectUrl=https://test.com/failed");
        assertThat(params[8]).isEqualTo("cancelRedirectUrl=https%3A%2F%2Ftest.com%2Fcancelled");
        assertThat(decode(params[8])).isEqualTo("cancelRedirectUrl=https://test.com/cancelled");

        // Basket
        assertThat(params[9]).isEqualTo("basket%5B0%5D%5Bname%5D=Test%201");
        assertThat(decode(params[9])).isEqualTo("basket[0][name]=Test 1");
        assertThat(params[10]).isEqualTo("basket%5B0%5D%5Bquantity%5D=1");
        assertThat(decode(params[10])).isEqualTo("basket[0][quantity]=1");
        assertThat(params[11]).isEqualTo("basket%5B0%5D%5Bamount%5D=800");
        assertThat(decode(params[11])).isEqualTo("basket[0][amount]=800");
        assertThat(params[12]).isEqualTo("basket%5B1%5D%5Bname%5D=Test%202");
        assertThat(decode(params[12])).isEqualTo("basket[1][name]=Test 2");
        assertThat(params[13]).isEqualTo("basket%5B1%5D%5Bquantity%5D=1");
        assertThat(decode(params[13])).isEqualTo("basket[1][quantity]=1");
        assertThat(params[14]).isEqualTo("basket%5B1%5D%5Bamount%5D=200");
        assertThat(decode(params[14])).isEqualTo("basket[1][amount]=200");

        assertThat(params[15]).isEqualTo("psp%5B0%5D=27");
        assertThat(decode(params[15])).isEqualTo("psp[0]=27");
        assertThat(params[16]).isEqualTo("preAuthorization=false");
        assertThat(params[17]).isEqualTo("reservation=false");
        assertThat(params[18]).isEqualTo("referenceId=Reference12345");

        // Fields
        assertThat(params[19]).isEqualTo("fields%5Bforename%5D%5Bvalue%5D=Max");
        assertThat(decode(params[19])).isEqualTo("fields[forename][value]=Max");
        assertThat(params[20]).isEqualTo("fields%5Bsurname%5D%5Bvalue%5D=M%C3%BCller");
        assertThat(decode(params[20])).isEqualTo("fields[surname][value]=Müller");
        assertThat(params[21]).isEqualTo("fields%5Bemail%5D%5Bvalue%5D=test%40email.com");
        assertThat(decode(params[21])).isEqualTo("fields[email][value]=test@email.com");

        assertThat(params[22]).isEqualTo("skipResultPage=false");
        assertThat(params[23]).isEqualTo("chargeOnAuthorization=false");
        assertThat(params[24]).isEqualTo("validity=10");
        assertThat(params[25]).isEqualTo("subscriptionState=false");
        assertThat(params[26]).isEqualTo("buttonText%5B0%5D=Button%20Text");
        assertThat(decode(params[26])).isEqualTo("buttonText[0]=Button Text");
        assertThat(params[27]).isEqualTo("lookAndFeelProfile=2e56a19b");
        assertThat(params[28]).isEqualTo("successMessage=Great%20Success%20%3A-%29");
        assertThat(decode(params[28])).isEqualTo("successMessage=Great Success :-)");

        // API Signature
        assertThat(params[29]).isEqualTo("ApiSignature=S17MpExRfP2e%2FY%2B06LnVpv%2BmkbtfqabPbbdrxUwAoAI%3D");
    }

    @Test
    public void testGetGateway() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("gateway_confirmed_response.json");

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.GATEWAY)
            .action(PayrexxAction.GET)
            .id("123456789")
            .build();

        List<GatewayResponse> response = executeTestRequest(req, GatewayResponse.class);
        assertThat(response).hasSize(1);
        assertThat(response).containsExactly(CONFIRMED_RESPONSE);

        assertGetRequestWithoutBody(requestCapture.getValue(), "/v1.0/Gateway/123456789");
    }

    @Test
    public void testDeleteGateway() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("delete_response.json");

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.GATEWAY)
            .action(PayrexxAction.DELETE)
            .id("123456789")
            .build();

        List<GatewayResponse> response = executeTestRequest(req, GatewayResponse.class);
        assertThat(response).hasSize(1);
        assertThat(response).containsExactly(DELETE_RESPONSE);

        assertDeleteRequest(requestCapture.getValue(), "/v1.0/Gateway/123456789");
    }
}
