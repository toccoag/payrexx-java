package ch.tocco.payrexx;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.util.Collections;
import java.util.List;

import org.testng.annotations.*;
import org.easymock.Capture;

import ch.tocco.payrexx.model.request.InvoiceRequest;
import ch.tocco.payrexx.model.response.InvoiceResponse;
import ch.tocco.payrexx.types.PayrexxAction;
import ch.tocco.payrexx.types.PayrexxEndpoint;

import static com.google.common.truth.Truth.*;

public class InvoiceTest extends AbstractPayrexxTest {
    private static final InvoiceResponse MINIMAL_RESPONSE = new InvoiceResponse(123456789,
        "waiting",
        "25f9e794323b453885f5181f1b624d0b",
        "Reference12345",
        "https://test-instance.payrexx.com/?payment=25f9e794323b453885f5181f1b624d0b",
        false,
        "",
        true,
        List.of(36,27),
        Collections.emptyList(),
        1000,
        null,
        "CHF",
        null
        );

    private static final InvoiceResponse EXTENDED_RESPONSE = new InvoiceResponse(123456789,
        "waiting",
        "25f9e794323b453885f5181f1b624d0b",
        "Reference12345",
        "https://test-instance.payrexx.com/?payment=25f9e794323b453885f5181f1b624d0b",
        false,
        "Test Name",
        true,
        List.of(36),
        List.of("mastercard"),
        1000,
        8.0,
        "CHF",
        "sku"
    );

    private static final InvoiceResponse DELETE_RESPONSE = new InvoiceResponse(123456789,
        null,
        null,
        null,
        null,
        false,
        null,
        false,
        null,
        null,
        0,
        null,
        null,
        null
    );

    @Test
    public void testCreateMinimalInvoice() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("invoice_minimal_response.json");

        InvoiceRequest invoiceRequest = new InvoiceRequest.Builder()
            .title("Title")
            .description("Description")
            .referenceId("Reference12345")
            .purpose("Purpose")
            .amount(1000)
            .currency("CHF")
            .build();

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.INVOICE)
            .action(PayrexxAction.CREATE)
            .bodyData(invoiceRequest)
            .build();

        List<InvoiceResponse> resp = executeTestRequest(req, InvoiceResponse.class);
        assertThat(resp).hasSize(1);
        assertThat(resp).containsExactly(MINIMAL_RESPONSE);

        HttpRequest httpRequest = requestCapture.getValue();
        assertCreateRequest(httpRequest, "/v1.0/Invoice");

        // Assert Body
        String bodyValue = retrieveBodyString(httpRequest);
        String[] params = bodyValue.split("&");

        assertThat(params[0]).isEqualTo("model=Invoice");
        assertThat(params[1]).isEqualTo("title=Title");
        assertThat(params[2]).isEqualTo("description=Description");
        assertThat(params[3]).isEqualTo("referenceId=Reference12345");
        assertThat(params[4]).isEqualTo("purpose=Purpose");
        assertThat(params[5]).isEqualTo("amount=1000");
        assertThat(params[6]).isEqualTo("currency=CHF");

        // API Signature
        assertThat(params[7]).isEqualTo("ApiSignature=MBdRmKCZo2%2BQaRApGzwv4Vi5eG3x0INynTOP7QFZfVg%3D");
    }

    @Test
    public void testCreateExtendedInvoice() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("invoice_extended_response.json");

        InvoiceRequest invoiceRequest = new InvoiceRequest.Builder()
            .title("Title")
            .description("Test Description")
            .referenceId("Reference12345")
            .purpose("Purpose")
            .amount(1000)
            .currency("CHF")
            .vatRate(8.0)
            .psp(List.of(36))
            .pm(List.of("mastercard"))
            .sku("sku")
            .preAuthorization(false)
            .reservation(false)
            .name("Test Name")
            .field("forename", true)
            .field("surname", true)
            .field("email", true)
            .hideFields(false)
            .concardisOrderId("54321")
            .buttonText("Button Text :-)")
            .expirationDate("2023-12-31")
            .successRedirectUrl("https://tocco.ch/success")
            .failedRedirectUrl("https://tocco.ch/failed")
            .subscriptionState(false)
            .build();

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.INVOICE)
            .action(PayrexxAction.CREATE)
            .bodyData(invoiceRequest)
            .build();

        List<InvoiceResponse> resp = executeTestRequest(req, InvoiceResponse.class);
        assertThat(resp).hasSize(1);
        assertThat(resp).containsExactly(EXTENDED_RESPONSE);

        HttpRequest httpRequest = requestCapture.getValue();
        assertCreateRequest(httpRequest, "/v1.0/Invoice");

        // Assert Body
        String bodyValue = retrieveBodyString(httpRequest);
        String[] params = bodyValue.split("&");

        assertThat(params[0]).isEqualTo("model=Invoice");
        assertThat(params[1]).isEqualTo("title=Title");
        assertThat(params[2]).isEqualTo("description=Test%20Description");
        assertThat(params[3]).isEqualTo("referenceId=Reference12345");
        assertThat(params[4]).isEqualTo("purpose=Purpose");
        assertThat(params[5]).isEqualTo("amount=1000");
        assertThat(params[6]).isEqualTo("currency=CHF");
        assertThat(params[7]).isEqualTo("vatRate=8.0");
        assertThat(params[8]).isEqualTo("psp%5B0%5D=36");
        assertThat(decode(params[8])).isEqualTo("psp[0]=36");
        assertThat(params[9]).isEqualTo("pm%5B0%5D=mastercard");
        assertThat(decode(params[9])).isEqualTo("pm[0]=mastercard");
        assertThat(params[10]).isEqualTo("sku=sku");
        assertThat(params[11]).isEqualTo("preAuthorization=false");
        assertThat(params[12]).isEqualTo("reservation=false");
        assertThat(params[13]).isEqualTo("name=Test%20Name");

        // Fields
        assertThat(params[14]).isEqualTo("fields%5Bforename%5D%5Bmandatory%5D=true");
        assertThat(decode(params[14])).isEqualTo("fields[forename][mandatory]=true");
        assertThat(params[15]).isEqualTo("fields%5Bsurname%5D%5Bmandatory%5D=true");
        assertThat(decode(params[15])).isEqualTo("fields[surname][mandatory]=true");
        assertThat(params[16]).isEqualTo("fields%5Bemail%5D%5Bmandatory%5D=true");
        assertThat(decode(params[16])).isEqualTo("fields[email][mandatory]=true");

        assertThat(params[17]).isEqualTo("hideFields=false");
        assertThat(params[18]).isEqualTo("concardisOrderId=54321");
        assertThat(params[19]).isEqualTo("buttonText=Button%20Text%20%3A-%29");
        assertThat(decode(params[19])).isEqualTo("buttonText=Button Text :-)");
        assertThat(params[20]).isEqualTo("expirationDate=2023-12-31");
        assertThat(params[21]).isEqualTo("successRedirectUrl=https%3A%2F%2Ftocco.ch%2Fsuccess");
        assertThat(decode(params[21])).isEqualTo("successRedirectUrl=https://tocco.ch/success");
        assertThat(params[22]).isEqualTo("failedRedirectUrl=https%3A%2F%2Ftocco.ch%2Ffailed");
        assertThat(decode(params[22])).isEqualTo("failedRedirectUrl=https://tocco.ch/failed");
        assertThat(params[23]).isEqualTo("subscriptionState=false");

        // API Signature
        assertThat(params[24]).isEqualTo("ApiSignature=tykuldUkXLOxaBwj42ipUh5%2FA5IBA9fapB3Q1tXiXyc%3D");
    }

    @Test
    public void testGetInvoice() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("invoice_minimal_response.json");

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.INVOICE)
            .action(PayrexxAction.GET)
            .id("123456789")
            .build();

        List<InvoiceResponse> resp = executeTestRequest(req, InvoiceResponse.class);
        assertThat(resp).hasSize(1);
        assertThat(resp).containsExactly(MINIMAL_RESPONSE);

        assertGetRequestWithoutBody(requestCapture.getValue(), "/v1.0/Invoice/123456789");
    }

    @Test
    public void testDeleteInvoice() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("delete_response.json");

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.INVOICE)
            .action(PayrexxAction.DELETE)
            .id("123456789")
            .build();

        List<InvoiceResponse> resp = executeTestRequest(req, InvoiceResponse.class);
        assertThat(resp).hasSize(1);
        assertThat(resp).containsExactly(DELETE_RESPONSE);

        assertDeleteRequest(requestCapture.getValue(), "/v1.0/Invoice/123456789");
    }
}
