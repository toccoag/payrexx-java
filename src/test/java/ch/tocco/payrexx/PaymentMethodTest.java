package ch.tocco.payrexx;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.util.List;
import java.util.Map;

import org.testng.annotations.*;
import org.easymock.Capture;

import com.google.common.truth.Correspondence;

import ch.tocco.payrexx.model.request.PaymentMethodRequest;
import ch.tocco.payrexx.model.response.PaymentMethodResponse;
import ch.tocco.payrexx.types.PayrexxAction;
import ch.tocco.payrexx.types.PayrexxEndpoint;

import static com.google.common.truth.Truth.*;

public class PaymentMethodTest extends AbstractPayrexxTest {
    private static final List<Map<String, Object>> ALL_PM = List.of(
        Map.of("id", "alipay",
            "name", "Alipay",
            "label", "Alipay",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_alipay.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time"),
            "currencies", List.of("CHF")),
        Map.of("id", "american-express",
            "name", "American Express",
            "label", "American Express",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_american-express.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time", "reservation", "authorization"),
            "currencies", List.of("CHF")),
        Map.of("id", "apple-pay",
            "name", "Apple Pay",
            "label", "Apple Pay",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_apple-pay.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time"),
            "currencies", List.of("CHF")),
        Map.of("id", "bancontact",
            "name", "Bancontact",
            "label", "Bancontact",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_bancontact.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time"),
            "currencies", List.of("CHF")),
        Map.of("id", "eps",
            "name", "EPS",
            "label", "EPS",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_eps.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time"),
            "currencies", List.of("CHF")),
        Map.of("id", "giropay",
            "name", "giropay",
            "label", "giropay",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_giropay.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time"),
            "currencies", List.of("CHF")),
        Map.of("id", "google-pay",
            "name", "Google Pay",
            "label", "Google Pay",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_google-pay.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time"),
            "currencies", List.of("CHF")),
        Map.of("id", "ideal",
            "name", "iDEAL",
            "label", "iDeal",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_ideal.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time"),
            "currencies", List.of("CHF")),
        Map.of("id", "mastercard",
            "name", "Mastercard",
            "label", "Mastercard",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_mastercard.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time", "reservation", "authorization"),
            "currencies", List.of("CHF")),
        Map.of("id", "p24",
            "name", "Przelewy24",
            "label", "Przelewy24",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_p24.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time"),
            "currencies", List.of("CHF")),
        Map.of("id", "sofort",
            "name", "Sofort",
            "label", "Sofort",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_sofort.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time"),
            "currencies", List.of("CHF")),
        Map.of("id", "visa",
            "name", "Visa",
            "label", "Visa",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_visa.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time", "reservation", "authorization"),
            "currencies", List.of("CHF")),
        Map.of("id", "wechat-pay",
            "name", "WeChat Pay",
            "label", "WeChat Pay",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_wechat-pay.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time"),
            "currencies", List.of("CHF"))
    );

    private static final List<Map<String, Object>> FILTERED_PM = List.of(
        Map.of("id", "american-express",
            "name", "American Express",
            "label", "American Express",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_american-express.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time", "reservation", "authorization"),
            "currencies", List.of("CHF")),
        Map.of("id", "mastercard",
            "name", "Mastercard",
            "label", "Mastercard",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_mastercard.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time", "reservation", "authorization"),
            "currencies", List.of("CHF")),
        Map.of("id", "visa",
            "name", "Visa",
            "label", "Visa",
            "logo", "https://media.payrexx.com/assets/cardIcons/card_visa.svg",
            "psp", "36",
            "mode", "test",
            "paymentTypes", List.of("one-time", "reservation", "authorization"),
            "currencies", List.of("CHF"))
    );

    @Test
    public void testGetAllPaymentMethods() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("payment_method_all_response.json");

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.PAYMENT_METHOD)
            .action(PayrexxAction.GET)
            .build();

        List<PaymentMethodResponse> resp = executeTestRequest(req, PaymentMethodResponse.class);
        assertThat(resp).hasSize(13);

        assertThat(resp)
            .comparingElementsUsing(Correspondence.from(this::comparePaymentMethod, "comparePaymentMethods"))
            .containsExactlyElementsIn(ALL_PM);

        HttpRequest httpRequest = requestCapture.getValue();
        assertGetRequestWithoutBody(httpRequest, "/v1.0/PaymentMethod");
    }

    @Test
    public void testGetFilteredPaymentMethods() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("payment_method_filtered_response.json");

        PaymentMethodRequest pmr = new PaymentMethodRequest.Builder()
            .filterCurrency("CHF")
            .filterPsp(36)
            .filterPaymentType("reservation")
            .build();

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.PAYMENT_METHOD)
            .action(PayrexxAction.GET)
            .bodyData(pmr)
            .build();

        List<PaymentMethodResponse> resp = executeTestRequest(req, PaymentMethodResponse.class);
        assertThat(resp).hasSize(3);

        assertThat(resp)
            .comparingElementsUsing(Correspondence.from(this::comparePaymentMethod, "comparePaymentMethods"))
            .containsExactlyElementsIn(FILTERED_PM);

        HttpRequest httpRequest = requestCapture.getValue();
        assertGetRequest(httpRequest, "/v1.0/PaymentMethod");

        String bodyValue = retrieveBodyString(httpRequest);
        String[] params = bodyValue.split("&");

        assertThat(params[0]).isEqualTo("model=PaymentMethod");
        assertThat(params[1]).isEqualTo("filterCurrency=CHF");
        assertThat(params[2]).isEqualTo("filterPaymentType=reservation");
        assertThat(params[3]).isEqualTo("filterPsp=36");
        assertThat(params[4]).isEqualTo("ApiSignature=GMslMU%2FlbreGiNw6eM0xZ%2F40b060qDxn0IIYtX6kGxs%3D");
    }

    private boolean comparePaymentMethod(PaymentMethodResponse pmr, Map<String, Object> expectedValues) {
        if (!expectedValues.get("id").equals(pmr.id())) {
            return false;
        }
        if (!expectedValues.get("name").equals(pmr.name())) {
            return false;
        }
        if (!expectedValues.get("label").equals(pmr.label().get("en"))) {
            return false;
        }
        if (!expectedValues.get("logo").equals(pmr.logo().get("en"))) {
            return false;
        }
        String psp = (String) expectedValues.get("psp");
        if (!pmr.optionsByPsp().containsKey(psp)) {
            return false;
        }
        if (!expectedValues.get("mode").equals(pmr.optionsByPsp().get(psp).mode())) {
            return false;
        }
        if (!expectedValues.get("paymentTypes").equals(pmr.optionsByPsp().get(psp).paymentTypes())) {
            return false;
        }
        if (!expectedValues.get("currencies").equals(pmr.optionsByPsp().get(psp).currencies())) {
            return false;
        }
        return true;
    }
}
