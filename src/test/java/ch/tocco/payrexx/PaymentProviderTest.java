package ch.tocco.payrexx;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.util.Collections;
import java.util.List;

import org.testng.annotations.*;
import org.easymock.Capture;

import ch.tocco.payrexx.model.response.PaymentProviderResponse;
import ch.tocco.payrexx.types.PayrexxAction;
import ch.tocco.payrexx.types.PayrexxEndpoint;

import static com.google.common.truth.Truth.*;

public class PaymentProviderTest extends AbstractPayrexxTest {

    @Test
    public void testGetPaymentProviders() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("psp_response.json");

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.PAYMENT_PROVIDER)
            .action(PayrexxAction.GET)
            .build();

        List<PaymentProviderResponse> response = executeTestRequest(req, PaymentProviderResponse.class);
        assertThat(response).hasSize(1);
        assertThat(response).containsExactly(new PaymentProviderResponse(27,
            "Vorkasse",
            Collections.emptyList(),
            Collections.emptyList()));

        assertGetRequestWithoutBody(requestCapture.getValue(), "/v1.0/PaymentProvider");
    }
}
