package ch.tocco.payrexx;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.util.List;

import org.testng.annotations.*;
import org.easymock.Capture;

import ch.tocco.payrexx.model.response.SignatureCheckResponse;
import ch.tocco.payrexx.types.PayrexxAction;
import ch.tocco.payrexx.types.PayrexxEndpoint;

import static com.google.common.truth.Truth.*;

public class SignatureCheckTest extends AbstractPayrexxTest {

    @Test
    public void testSignatureCheck() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("signature_check_response.json");

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.SIGNATURE_CHECK)
            .action(PayrexxAction.GET)
            .build();

        List<SignatureCheckResponse> response = executeTestRequest(req, SignatureCheckResponse.class);
        assertThat(response).hasSize(1);
        assertThat(response).containsExactly(new SignatureCheckResponse(1));

        assertGetRequestWithoutBody(requestCapture.getValue(), "/v1.0/SignatureCheck");
    }

    @Test(expectedExceptions = PayrexxException.class)
    public void testErrorResponse() throws IOException, InterruptedException, PayrexxException {
        expectRequest("api_secret_error.json");

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.SIGNATURE_CHECK)
            .action(PayrexxAction.GET)
            .build();

        executeTestRequest(req, SignatureCheckResponse.class);
    }
}
