package ch.tocco.payrexx;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.util.List;

import org.testng.annotations.*;
import org.easymock.Capture;

import ch.tocco.payrexx.model.response.TransactionResponse;
import ch.tocco.payrexx.types.PayrexxAction;
import ch.tocco.payrexx.types.PayrexxEndpoint;

import static com.google.common.truth.Truth.*;

public class TransactionTest extends AbstractPayrexxTest {
    private static final TransactionResponse TRANSACTION_RESPONSE = new TransactionResponse(123456879,
        "5dbce7a3",
        136000,
        "Reference12345",
        "2023-12-14 16:08:45",
        "confirmed",
        "de",
        "Payrexx_Payments_DIRECT",
        36,
        "TEST");

    @Test
    public void testGetTransaction() throws IOException, InterruptedException, PayrexxException {
        Capture<HttpRequest> requestCapture = expectRequest("transaction_response.json");

        PayrexxRequest req = new PayrexxRequest.Builder()
            .endpoint(PayrexxEndpoint.TRANSACTION)
            .action(PayrexxAction.GET)
            .id("123456879")
            .build();

        List<TransactionResponse> resp = executeTestRequest(req, TransactionResponse.class);
        assertThat(resp).hasSize(1);
        assertThat(resp).containsExactly(TRANSACTION_RESPONSE);

        HttpRequest httpRequest = requestCapture.getValue();
        assertGetRequestWithoutBody(httpRequest, "/v1.0/Transaction/123456879");
    }
}
